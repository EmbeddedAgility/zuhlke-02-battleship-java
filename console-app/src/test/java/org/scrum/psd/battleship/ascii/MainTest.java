package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;

@Execution(ExecutionMode.CONCURRENT)
public class MainTest {
    @Test
    public void testValidPosition() {
        Position actual = Main.parsePosition("A1");
        Position expected = new Position(Letter.A, 1);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testInvalidPositionWithInvalidLetter() {
        Position actual = Main.parsePosition("Z1");

        Assertions.assertNull(actual);
    }

    @Test
    public void testInvalidPositionWithInvalidNumber() {
        Position actual = Main.parsePosition("B10");

        Assertions.assertNull(actual);
    }

    @Test
    public void testInvalidPositionWithInvalidLetterAndNumber() {
        Position actual = Main.parsePosition("Z100");

        Assertions.assertNull(actual);
    }
}
