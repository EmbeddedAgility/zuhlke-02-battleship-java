package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.Clip;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;
    private static ColoredPrinter console = new ColoredPrinter.Builder(1, false).background(Ansi.BColor.BLACK).foreground(Ansi.FColor.WHITE).build();

    public static void main(String[] args) {
        console.setForegroundColor(Ansi.FColor.MAGENTA);
        console.println("                                     |__");
        console.println("                                     |\\/");
        console.println("                                     ---");
        console.println("                                     / | [");
        console.println("                              !      | |||");
        console.println("                            _/|     _/|-++'");
        console.println("                        +  +--|    |--|--|_ |-");
        console.println("                     { /|__|  |/\\__|  |--- |||__/");
        console.println("                    +---------------___[}-_===_.'____                 /\\");
        console.println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        console.println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        console.println("|                        Welcome to Battleship                         BB-61/");
        console.println(" \\_________________________________________________________________________|");
        console.println("");
        console.setForegroundColor(Ansi.FColor.YELLOW);

        InitializeGame();

        StartGame();
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        console.setForegroundColor(Ansi.FColor.YELLOW);
        console.print("\033[2J\033[;H");
        console.println("                  __");
        console.println("                 /  \\");
        console.println("           .-.  |    |");
        console.println("   *    _.-'  \\  \\__/");
        console.println("    \\.-'       \\");
        console.println("   /          _/");
        console.println("  |      _  /\" \"");
        console.println("  |     /_\'");
        console.println("   \\    \\_/");
        console.println("    \" \"\" \"\" \"\" \"");

        Boolean gameOver = false;

        do {
            separationLine();
            Position position = null;
            do {
                console.println("Player, it's your turn");
                console.println("Enter coordinates for your shot :");
                position = parsePosition(scanner.next());
            } while(null == position);
            boolean isHit = GameController.checkIsHit(enemyFleet, position);
            Ship enemyShipHit = getEnemyHitShip(position);

            if (isHit) {
                beep();
                console.setForegroundColor(Ansi.FColor.GREEN);
                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");
                console.setForegroundColor(Ansi.FColor.YELLOW);
            }

            printColoredString(isHit, isHit ? "Yeah ! Nice hit !" : "Miss");
            if (enemyShipHit != null && enemyShipHit.isSunk()) {
                printColoredString(true, "You sunk the ship: " + enemyShipHit.getName());
                printColoredString(true, "Sunk ships:");
                for (Ship s : enemyFleet) {
                    if (s.isSunk()) {
                        printColoredString(true, "\tShip name: " + s.getName());
                    }
                }
            }
            separationLine();
            position = getRandomPosition();
            isHit = GameController.checkIsHit(myFleet, position);
            String message = String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), isHit ? "hit your ship !" : "miss");
            printColoredString(!isHit, message);
            if (isHit) {
                beep();
                console.setForegroundColor(Ansi.FColor.RED);
                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");
                console.setForegroundColor(Ansi.FColor.YELLOW);

            }

            if(isGameOver()){
                Boolean win = isWin();
                printCoolEnding(win);
                gameOver = true;
            }

        } while (!gameOver);
    }

    private static Ship getEnemyHitShip(Position position) {
        for (Ship ship : enemyFleet) {
            for (Position enemyShipPosition : ship.getPositions()) {
                if (enemyShipPosition.equals(position)) {
                    ship.markPositionHit(position);
                    return ship;
                }
            }
        }
        return null;
    }

    private static void beep() {
        console.print("\007");
    }

    protected static Position parsePosition(String input) {
        try {
            Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
            int number = Integer.parseInt(input.substring(1));

            if(number < 0 || number > 8){
                throw new IllegalArgumentException("Invalid boundary");
            }

            return new Position(letter, number);
        } catch(IllegalArgumentException iae) {
            printColoredString(false, "Invalid input position provided!");
            return null;
        }
    }

    private static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        Position position = new Position(letter, number);
        return position;
    }

    private static void InitializeGame() {
        InitializeMyFleet();

        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        console.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet) {
            console.println("");
            console.println(String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
            for (int i = 1; i <= ship.getSize(); i++) {
                Position shipPosition = enterShipPosition(scanner, i, ship);
                ship.addPosition(shipPosition);
            }
            separationLine();
        }
    }

    private static Position enterShipPosition(Scanner scanner, int index, Ship ship) {
        Position position = null;
        do {
            console.println(String.format("Enter position %s of %s (i.e A3):", index, ship.getSize()));
            position = parsePosition(scanner.next());
        } while(null == position);

        return position;
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 5));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }

    private static void printColoredString(Boolean success, String message){
        console.setForegroundColor(success ? Ansi.FColor.GREEN : Ansi.FColor.RED);
        console.println(message);
        console.setForegroundColor(Ansi.FColor.YELLOW);
    }

    private static Boolean isGameOver(){
        return isFleetSunk(myFleet) || isFleetSunk(enemyFleet);
    }

    private static Boolean isWin(){
        return isFleetSunk(enemyFleet);
    }

    private static Boolean isFleetSunk(List<Ship> fleet){
        Boolean lost = true;
        for (Ship s: fleet){
            if(!s.isSunk()){
                lost = false;
            }
        }
        return lost;
    }

    private static void printCoolEnding(Boolean win){
        console.setForegroundColor(win ? Ansi.FColor.GREEN : Ansi.FColor.RED);
        if (win){
            playSound("win.wav");
            console.println("  ___                                  ___  ");
            console.println(" (o o)                                (o o) ");
            console.println("(  V  ) WINNER WINNER CHICKEN DINNER (  V  )");
            console.println("--m-m----------------------------------m-m--");
        }else{
            playSound("loss.wav");
            console.println("▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄");
            console.println("██░███░█▀▄▄▀█░██░███░██▀▄▄▀█░▄▄█▄░▄███▄░▄█▀▄▄▀███░▄▄▀███▀▄▀█▀▄▄▀█░▄▀▄░█▀▄▄▀█░██░█▄░▄█░▄▄█░▄▄▀███░██▀▄▄▀█░█");
            console.println("██▄▀▀▀▄█░██░█░██░███░██░██░█▄▄▀██░█████░██░██░███░▀▀░███░█▀█░██░█░█▄█░█░▀▀░█░██░██░██░▄▄█░▀▀▄███░██░██░█░█");
            console.println("████░████▄▄███▄▄▄███▄▄██▄▄██▄▄▄██▄█████▄███▄▄████▄██▄████▄███▄▄██▄███▄█░█████▄▄▄██▄██▄▄▄█▄█▄▄███▄▄██▄▄██▄▄");
            console.println("▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀");
        }
        console.setForegroundColor(Ansi.FColor.YELLOW);
        try{
            Thread.sleep(5000);
        }catch(Exception e){
            System.err.println(e);
        }
    }

    public static synchronized void playSound(final String url) {
        new Thread(new Runnable() {
          public void run() {
            try {
              Clip clip = AudioSystem.getClip();
              AudioInputStream inputStream = AudioSystem.getAudioInputStream(
                Main.class.getResourceAsStream("./assets/" + url));
              clip.open(inputStream);
              clip.start(); 
            } catch (Exception e) {
              System.err.println(e.getMessage());
            }
          }
        }).start();
    }


    private static void separationLine() {
        console.println("-------------------------------------------------------------------");
    }
}
