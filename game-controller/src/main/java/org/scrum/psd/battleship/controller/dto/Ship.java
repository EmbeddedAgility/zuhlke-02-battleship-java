package org.scrum.psd.battleship.controller.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Ship {
    private boolean isPlaced;
    private String name;
    private int size;
    private Set<Position> positions;
    private Color color;

    public Ship() {
        this.positions = new HashSet<>();
    }

    public Ship(String name, int size) {
        this();

        this.name = name;
        this.size = size;
    }

    public Ship(String name, int size, List<Position> positions) {
        this(name, size);

        this.positions = new HashSet<>(positions);
    }

    public Ship(String name, int size, Color color) {
        this(name, size);

        this.color = color;
    }

    public void addPosition(Position position) {
        if (positions == null) {
            positions = new HashSet<>();
        }

        positions.add(position);
    }

    public boolean isSunk() {
        for (Position p : positions) {
            if (!p.isHit()) {
                return false;
            }
        }
        return true;
    }

    // TODO: property change listener implementieren

    public boolean isPlaced() {
        return isPlaced;
    }

    public void setPlaced(boolean placed) {
        isPlaced = placed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Position> getPositions() {
        return positions;
    }

    public void markPositionHit(Position position) {
        for (Position p : this.positions) {
            if (p.equals(position)) {
                p.setHit(true);
            }
        }
    }

    public void setPositions(List<Position> positions) {
        this.positions = new HashSet<>(positions);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
